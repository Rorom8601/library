package co.simplon.promo18.library.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.library.entities.Book;
import co.simplon.promo18.library.entities.Copy;

@Repository
public class CopyRepository {
  @Autowired
  private DataSource dataSource;

  @Autowired
  private BookRepository bookRepo;

  public List<Copy> findAll() {
    List<Copy> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM copy");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Copy copy = new Copy(rs.getInt("id"), rs.getString("editor"), rs.getInt("page_number"),
            rs.getString("state"), rs.getString("cover"), rs.getInt("stock"));
        list.add(copy);
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }
    return list;
  }

  public boolean decreaseStockWhenBorrowing(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("update copy SET stock=(stock-1) WHERE id=?");

      stmt.setInt(1, id);

      return stmt.executeUpdate() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
     
    }
  }


  public boolean modifyStock(int id, int nb) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("update copy SET stock=(stock+?) WHERE id=?");
      stmt.setInt(1, nb);
      stmt.setInt(2, id);

      return stmt.executeUpdate() == 1;

    } catch (SQLException e) {
     e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }
   
  }

  public boolean modifyState(String newState, int idCopy) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("update copy SET state=? WHERE id=?");
      stmt.setString(1, newState);
      stmt.setInt(2, idCopy);
      return stmt.executeUpdate() == 1;

    } catch (SQLException e) {
     e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }
  }



  public Copy findById(int id) {

    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM copy WHERE copy.id=?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Copy copy = new Copy(rs.getInt("id"), rs.getString("editor"), rs.getInt("page_number"),
            rs.getString("state"), rs.getString("cover"), rs.getInt("stock"));
        return copy;
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }
    return null;
  }

  public Copy findByIdWithBook(int id) {

    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM copy WHERE copy.id=?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Book book = bookRepo.getBookById(rs.getInt("book_id"));
        Copy copy = new Copy(rs.getInt("id"), rs.getString("editor"), rs.getInt("page_number"),
            rs.getString("state"), rs.getString("cover"), rs.getInt("stock"));
        copy.setBook(book);
        return copy;
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }
    return null;
  }

  // public List<Copy> findByBookId(int id) {
  //   List<Copy> list = new ArrayList<>();
  //   try (Connection connection = dataSource.getConnection()) {
  //     PreparedStatement stmt = connection.prepareStatement(
  //         "SELECT * FROM copy INNER JOIN book ON copy.book_id=book.id WHERE book.id=?");
  //     stmt.setInt(1, id);
  //     ResultSet rs = stmt.executeQuery();

  //     while (rs.next()) {
  //       Copy copy = new Copy(rs.getInt("id"), rs.getString("editor"), rs.getInt("page_number"),
  //           rs.getString("state"), rs.getString("cover"), rs.getInt("stock"));
  //       Book book = bookRepo.getBookById(id);
  //       copy.setBook(book);
  //       list.add(copy);
  //     }
  //   } catch (SQLException e) {

  //     e.printStackTrace();
  //     throw new RuntimeException("Database access error", e);
  //   }
  //   return list;
  // }
  public List<Copy> findByBookId(int id) {
    List<Copy> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "SELECT * FROM copy INNER JOIN book ON copy.book_id=book.id WHERE book.id=?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Copy copy = new Copy(rs.getInt("copy.id"), rs.getString("editor"), rs.getInt("page_number"),
            rs.getString("state"), rs.getString("cover"), rs.getInt("stock"));
        Book book = new Book(rs.getInt("book.id"), rs.getString("author"), rs.getString("title"), rs.getString("abstract"));
        copy.setBook(book);
        list.add(copy);
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }
    return list;
  }

  public void save(Copy copy) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "INSERT INTO copy (editor, page_number, state, cover, stock, book_id) VALUES (?,?,?,?,?,?)",
          PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setString(1, copy.getEditor());
      stmt.setInt(2, copy.getPageNumber());
      stmt.setString(3, copy.getState());
      stmt.setString(4, copy.getCover());
      stmt.setInt(5, copy.getStock());
      stmt.setInt(6, copy.getBook().getId());

      stmt.executeUpdate();
      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {

        copy.setId(rs.getInt(1));
      }

    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }

  }

  public boolean deleteById(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM copy WHERE id=?");

      stmt.setInt(1, id);

      return stmt.executeUpdate() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error", e);

    }
  }

  public boolean update(Copy copy) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("UPDATE copy SET editor=?,page_number=?, state=?, cover=?, stock=? WHERE id=?");

      stmt.setString(1, copy.getEditor());
      stmt.setInt(2, copy.getPageNumber());
      stmt.setString(3, copy.getState());
      stmt.setString(4, copy.getCover());
      stmt.setInt(5, copy.getStock());
      stmt.setInt(6, copy.getId());

      return stmt.executeUpdate() == 1;


    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error", e);

    }
  }

}

