package co.simplon.promo18.library.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.library.entities.Book;

@Repository
public class BookRepository {
  @Autowired
  private DataSource dataSource;

  public List<Book> findAll() {
    List<Book> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM book");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Book book = new Book(rs.getInt("id"), rs.getString("author"), rs.getString("title"),
            rs.getString("abstract"));
        list.add(book);
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }

    return list;
  }

  public List<Book> findBooksByCategory(int idCategory) {
    List<Book> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM book WHERE category_id= ?");
      stmt.setInt(1, idCategory);
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Book book = new Book(rs.getInt("id"), rs.getString("author"), rs.getString("title"),
            rs.getString("abstract"));
        list.add(book);
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }

    return list;
  }


  public List<Book> findByTitleOrAuthorLike(String search) {
    List<Book> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("select * from book WHERE book.title like ? OR author like ?");
      stmt.setString(1,"%"+search+"%");
      stmt.setString(2,"%"+search+"%");
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Book book = new Book(rs.getInt("id"), rs.getString("author"), rs.getString("title"),
            rs.getString("abstract"));
        list.add(book);
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }

    return list;
  }


  public Book save(Book book) {
      try (Connection connection = dataSource.getConnection()) {
          PreparedStatement stmt = connection.prepareStatement("INSERT INTO book (title, author, abstract, category_id) VALUES (?,?,?,?)",
                  PreparedStatement.RETURN_GENERATED_KEYS);

          stmt.setString(1,book.getTitle() );
          stmt.setString(2, book.getAuthor());
          stmt.setString(3,book.getSynopsis());
          stmt.setInt(4, book.getCategory().getId());

          stmt.executeUpdate();

          /**
           * cette partie là permet de récupérer l'id auto increment par la bdd et de
           * l'assigner à l'instance de Dog passée en argument
           */
          ResultSet rs = stmt.getGeneratedKeys();
          if (rs.next()) {

            book.setId(rs.getInt(1));
          }
          return book;

      } catch (SQLException e) {
          e.printStackTrace();
          throw new RuntimeException("Database access error", e);
      }
  }


  public Book getBookById(int id) {

    Book book = null;
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM book WHERE book.id= ?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        book = new Book(rs.getInt("id"), rs.getString("author"), rs.getString("title"), 
            rs.getString("abstract"));
      }

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }
    return book;

  }

  public boolean deleteById(int id) {
    try (Connection connection = dataSource.getConnection()) {
        PreparedStatement stmt = connection.prepareStatement("DELETE FROM book WHERE id=?");
  
        stmt.setInt(1, id);
  
        return stmt.executeUpdate() == 1;
  
    } catch (SQLException e) {
        e.printStackTrace();
        throw new RuntimeException("Database access error", e);
  
    }
  }

  public boolean update(Book book) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("UPDATE book SET title=?, author=?, abstract=? WHERE id=?");

      stmt.setString(1, book.getTitle());
      stmt.setString(2, book.getAuthor());
      stmt.setString(3, book.getSynopsis());
      stmt.setInt(4, book.getId());

      return stmt.executeUpdate() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }
  }

}
