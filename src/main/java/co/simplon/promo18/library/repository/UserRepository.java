package co.simplon.promo18.library.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.library.entities.User;

@Repository
public class UserRepository {

  @Autowired
  private DataSource dataSource;

  //Methode nécessaire pour l'authentification
  public User findByEmail(String email){
    User user = null;
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user WHERE user.email= ?");
      stmt.setString(1, email);
      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Date date = rs.getDate("birthdate");
        user = new User(rs.getInt("id"), rs.getString("first_name"), rs.getString("last_name"),
            rs.getString("email"), rs.getString("password"), rs.getString("role"),
           date==null?null: date.toLocalDate());
      }

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }
    return user;

  }

  public List<User> findByName(String searchField){
    List<User> list = new ArrayList();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("select * from user WHERE first_name like ? OR last_name like ?");
      stmt.setString(1, "%"+searchField+"%");
      stmt.setString(2, "%"+searchField+"%");
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Date date = rs.getDate("birthdate");
        User user = new User(rs.getInt("id"), rs.getString("first_name"), rs.getString("last_name"),
            rs.getString("email"), rs.getString("password"), rs.getString("role"),
           date==null?null: date.toLocalDate());
           list.add(user);
      }

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }
    return list;
  }
  

  public List<User> findAll() {
    List<User> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        User user = new User(rs.getInt("id"), rs.getString("first_name"), rs.getString("last_name"),
            rs.getString("email"), rs.getString("password"),rs.getString("role"),
            (rs.getDate("birthdate")).toLocalDate());
        list.add(user);
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }
    return list;
  }

  public User getUserById(int id) {

    User user = null;
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user WHERE user.id= ?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        user = new User(rs.getInt("id"), rs.getString("first_name"), rs.getString("last_name"),
            rs.getString("email"), rs.getString("password"), rs.getString("role"),
            (rs.getDate("birthdate")).toLocalDate());
      }

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }
    return user;
  }

  public void save(User user) {
    try (Connection connection = dataSource.getConnection()) {
        PreparedStatement stmt = connection.prepareStatement("INSERT INTO user ( email, password, role, first_name, last_name, birthdate) VALUES (?,?,?,?,?,?)",
                PreparedStatement.RETURN_GENERATED_KEYS);

        
        stmt.setString(1, user.getEmail());
        stmt.setString(2, user.getPassword());
        stmt.setString(3, user.getRole());
        stmt.setString(4,user.getFirstName() );
        stmt.setString(5, user.getLastName());
        stmt.setDate(6, Date.valueOf(user.getBirthdate()));

        stmt.executeUpdate();
       
        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) {

            user.setId(rs.getInt(1));
        }

    } catch (SQLException e) {
        e.printStackTrace();
        throw new RuntimeException("Database access error", e);
    }

}

public boolean completeAccount(User user){
  try (Connection connection = dataSource.getConnection()) {
    PreparedStatement stmt = connection.prepareStatement(
        "update user SET first_name=?, last_name=?, birthdate=? WHERE id=?",
        PreparedStatement.RETURN_GENERATED_KEYS);
    stmt.setString(1,user.getFirstName());
    stmt.setString(2, user.getLastName());
    stmt.setDate(3, Date.valueOf(user.getBirthdate()));
    stmt.setInt(4,user.getId());
    
   
  return  stmt.executeUpdate()==1;

  } catch (SQLException e) {
    e.printStackTrace();
    throw new RuntimeException("Database access error", e);
  }
}

}
