package co.simplon.promo18.library.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.library.entities.Borrowing;
import co.simplon.promo18.library.entities.Copy;

@Repository
public class BorrowingRepository {

  @Autowired
  private DataSource dataSource;

  @Autowired
  CopyRepository copyRepository;


  public List<Borrowing> findAll() {
    List<Borrowing> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM borrowing");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        if (rs.getString("returning_date") == null) {
          Borrowing borrowing =
              new Borrowing(rs.getInt("id"), (rs.getDate("borrowing_date")).toLocalDate(),
                  (rs.getDate("expected_due_date")).toLocalDate());
          list.add(borrowing);
        }

        else {
          Borrowing borrowing =
              new Borrowing(rs.getInt("id"), (rs.getDate("borrowing_date")).toLocalDate(),
                  (rs.getDate("returning_date")).toLocalDate(),
                  (rs.getDate("expected_due_date")).toLocalDate());
          list.add(borrowing);
        }

      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }

    return list;
  }

  public List<Borrowing> findBorrowingsPerUserId(int id) {
    List<Borrowing> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection
          .prepareStatement("select * from borrowing WHERE returning_date IS null AND user_id=?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        int copyId = rs.getInt("copy_id");
        Copy copy = copyRepository.findByIdWithBook(copyId);
        copy.getBook();
        Borrowing borrowing =
            new Borrowing(rs.getInt("id"), (rs.getDate("borrowing_date")).toLocalDate(),
                (rs.getDate("expected_due_date")).toLocalDate());
        borrowing.setCopy(copy);
        list.add(borrowing);

        // else {
        // Borrowing borrowing =
        // new Borrowing(rs.getInt("id"), (rs.getDate("borrowing_date")).toLocalDate(),
        // (rs.getDate("returning_date")).toLocalDate(),
        // (rs.getDate("expected_due_date")).toLocalDate() );
        // borrowing.setCopy(copy);
        // list.add(borrowing);
        // }

      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }

    return list;
  }

  public List<Borrowing> findCurrentBorrowingsPerUserId(int id) {
    List<Borrowing> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection
          .prepareStatement("select * from borrowing where returning_date IS null AND user_id=?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        int copyId = rs.getInt("copy_id");
        Copy copy = copyRepository.findByIdWithBook(copyId);
        copy.getBook();

        Borrowing borrowing =
            new Borrowing(rs.getInt("id"), (rs.getDate("borrowing_date")).toLocalDate(),
                (rs.getDate("expected_due_date")).toLocalDate());
        borrowing.setCopy(copy);
        list.add(borrowing);
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }

    return list;
  }

  public Borrowing findById(int id) {

    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("SELECT * FROM borrowing WHERE borrowing.id =?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Date returningDate = rs.getDate("returning_date");
        Borrowing borrowing =
            new Borrowing(rs.getInt("id"), (rs.getDate("borrowing_date")).toLocalDate(),
                (returningDate == null) ? null : (rs.getDate("returning_date")).toLocalDate(),
                (rs.getDate("expected_due_date")).toLocalDate());
        return borrowing;
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }

    return null;
  }

  public boolean save(Borrowing borrowing) {
    try (Connection connection = dataSource.getConnection()) {
      int copyId = borrowing.getCopy().getId();
      PreparedStatement stmt = connection.prepareStatement(
          "INSERT INTO borrowing (borrowing_date, returning_date, expected_due_date, copy_id, user_id) VALUES (?,?,?,?,?)",
          PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setDate(1, Date.valueOf(borrowing.getBorrowingDate()));
      stmt.setString(2, null);
      stmt.setDate(3, Date.valueOf(borrowing.getExpectedDueDate()));
      stmt.setInt(4, copyId);
      stmt.setInt(5, borrowing.getUser().getId());
      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        borrowing.setId(rs.getInt(1));
      }

    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
      
    }
    return true;
  }

  public boolean confirmReturnBorrowing(Borrowing borrowing) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("update borrowing SET returning_date=? WHERE id=?");

      stmt.setDate(1, Date.valueOf(borrowing.getReturningDate()));
      stmt.setInt(2, borrowing.getId());

      if (stmt.executeUpdate() == 1) {
        int idCopy = borrowing.getCopy().getId();
        copyRepository.modifyStock(idCopy, 1);
        return true;
      }

    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error", e);
    }
    return false;
  }
}
