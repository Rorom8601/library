package co.simplon.promo18.library.entities;

import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class Book {
  private Integer id;
  @NotBlank
  private String author;
  @NotBlank
  private String title;
  @NotBlank
  private String synopsis;
  private Category category;
  private List<Copy> copies;
  public Book() {}
  public Book(String author, String title, String synopsis) {
    this.author = author;
    this.title = title;
    this.synopsis=synopsis;
  }
  public Book(Integer id, String author, String title, String synopsis) {
    this.id = id;
    this.author = author;
    this.title = title;
    this.synopsis=synopsis;
  }
  public Integer getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getAuthor() {
    return author;
  }
  public void setAuthor(String author) {
    this.author = author;
  }
  public String getTitle() {
    return title;
  }
  public void setTitle(String title) {
    this.title = title;
  }
  public Category getCategory() {
    return category;
  }
  public void setCategory(Category category) {
    this.category = category;
  }
  public List<Copy> getCopies() {
    return copies;
  }
  public void setCopies(List<Copy> copies) {
    this.copies = copies;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public String getSynopsis() {
    return synopsis;
  }
  public void setSynopsis(String synopsis) {
    this.synopsis = synopsis;
  }
  
}
