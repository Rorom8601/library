package co.simplon.promo18.library.entities;

import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class Category {
  @NotNull
  private int id;
  @NotBlank
  private String name;
  private Set<Book> books=new HashSet<>();
  public Category() {}
  public Category(String name) {
    this.name = name;
  }
  public Category(int id, String name) {
    this.id = id;
    this.name = name;
  }
  public Integer getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  public Set<Book> getBooks() {
    return books;
  }
  public void setBooks(Set<Book> books) {
    this.books = books;
  }
  
}
