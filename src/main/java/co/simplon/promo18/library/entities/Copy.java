package co.simplon.promo18.library.entities;

import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

public class Copy {
  private Integer id;
  @NotBlank
  private String editor;
  @Positive
  private int pageNumber;
  @NotBlank
  private String state;
  @NotBlank
  private String cover;
  @PositiveOrZero
  private int stock;
  private Book book;
  private List<Borrowing> borrowings=new ArrayList<>();
  
  public Copy() {}

  public Copy(Integer id, String editor, int pageNumber, String state, String cover, int stock) {
    this.id = id;
    this.editor = editor;
    this.pageNumber = pageNumber;
    this.state = state;
    this.cover = cover;
    this.stock = stock;
  }
  
  public Copy(String editor, int pageNumber, String state, String cover, Book book,int stock ) {
    this.editor = editor;
    this.pageNumber = pageNumber;
    this.state = state;
    this.cover = cover;
    this.book = book;
    this.stock = stock;
  }
  public Copy(Integer id, String editor, int pageNumber, String state, String cover, Book book, int stock) {
    this.id = id;
    this.editor = editor;
    this.pageNumber = pageNumber;
    this.state = state;
    this.cover = cover;
    this.book = book;
    this.stock = stock;
  }
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public String getEditor() {
    return editor;
  }
  public void setEditor(String editor) {
    this.editor = editor;
  }
  public int getPageNumber() {
    return pageNumber;
  }
  public void setPageNumber(int pageNumber) {
    this.pageNumber = pageNumber;
  }
  public String getState() {
    return state;
  }
  public void setState(String state) {
    this.state = state;
  }
  public String getCover() {
    return cover;
  }
  public void setCover(String cover) {
    this.cover = cover;
  }
  public Book getBook() {
    return book;
  }
  public void setBook(Book book) {
    this.book = book;
  }
  public List<Borrowing> getBorrowings() {
    return borrowings;
  }
  public void setBorrowings(List<Borrowing> borrowings) {
    this.borrowings = borrowings;
  }

  public int getStock() {
    return stock;
  }

  public void setStock(int stock) {
    this.stock = stock;
  }
  
}
