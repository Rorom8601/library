package co.simplon.promo18.library.entities;

import java.time.LocalDate;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

public class Borrowing {

  private Integer id;
  @PastOrPresent
  private LocalDate borrowingDate;
  private LocalDate returningDate;
  @Future
  private LocalDate expectedDueDate;
  private Copy copy;
  private User user;

  public Borrowing() {}
  
  public Borrowing(Integer id, LocalDate borrowingDate, LocalDate expectedDueDate) {
    this.id = id;
    this.borrowingDate = borrowingDate;
    this.expectedDueDate=expectedDueDate;
  }
  public Borrowing(LocalDate borrowingDate, LocalDate returningDate, LocalDate expectedDueDate) {
    this.borrowingDate = borrowingDate;
    this.returningDate = returningDate;
    this.expectedDueDate=expectedDueDate;
  }
  public Borrowing(Integer id, LocalDate borrowingDate, LocalDate returningDate, LocalDate expectedDueDate) {
    this.id = id;
    this.borrowingDate = borrowingDate;
    this.returningDate = returningDate;
    this.expectedDueDate=expectedDueDate;
  }
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public LocalDate getBorrowingDate() {
    return borrowingDate;
  }
  public void setBorrowingDate(LocalDate borrowingDate) {
    this.borrowingDate = borrowingDate;
  }
  public LocalDate getReturningDate() {
    return returningDate;
  }
  public void setReturningDate(LocalDate returningDate) {
    this.returningDate = returningDate;
  }
  public Copy getCopy() {
    return copy;
  }
  public void setCopy(Copy copy) {
    this.copy = copy;
  }
  public User getUser() {
    return user;
  }
  public void setUser(User user) {
    this.user = user;
  }

  public LocalDate getExpectedDueDate() {
    return expectedDueDate;
  }

  public void setExpectedDueDate(LocalDate expectedDueDate) {
    this.expectedDueDate = expectedDueDate;
  }
}
