package co.simplon.promo18.library.auth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import co.simplon.promo18.library.repository.UserRepository;

@Service
public class AuthService implements UserDetailsService {

  @Autowired
  private UserRepository repo;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
      
    try {
      return repo.findByEmail(username);
    } catch (Exception e) {
      throw new UsernameNotFoundException("User not found");
    }
  }

  
}
