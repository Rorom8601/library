package co.simplon.promo18.library.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import co.simplon.promo18.library.entities.Book;
import co.simplon.promo18.library.repository.BookRepository;
import org.springframework.http.HttpStatus;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/book")
public class BookController {

  @Autowired
  private BookRepository bookRepo;

  @GetMapping
  public List<Book> getAll(){
    List<Book> bookList=bookRepo.findAll();
    return bookList;
  }

  @GetMapping("/category/{id}")
  public List<Book> getByCategory(@PathVariable int id){
    List<Book> bookList=bookRepo.findBooksByCategory(id);
    return bookList;
  }

  @GetMapping("/search/{field}")
  public List<Book> getBySeachField(@PathVariable String field){
    return bookRepo.findByTitleOrAuthorLike(field);
  }

  @GetMapping("/{id}")
  public Book getById(@PathVariable int id){
    Book book=bookRepo.getBookById(id);
    return book;
  }

  @PostMapping
  public Book createBook(@Valid @RequestBody Book book){
   return bookRepo.save(book);
  }

  
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @PutMapping
  public boolean updateBook(@RequestBody Book book){
    return bookRepo.update(book);
  }

  @DeleteMapping("/{id}")
  public boolean deleteBook(@PathVariable int id){
    return bookRepo.deleteById(id);
  }
}
