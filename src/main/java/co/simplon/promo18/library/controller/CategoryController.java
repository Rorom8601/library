package co.simplon.promo18.library.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import co.simplon.promo18.library.entities.Category;
import co.simplon.promo18.library.repository.CategoryRepository;
import org.springframework.http.HttpStatus;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/category")
public class CategoryController {

  @Autowired
  CategoryRepository catRepo;

  @GetMapping
  public List<Category> getAllCategories(){
    List<Category> catList=catRepo.findAll();
    return catList;
  }

  @GetMapping("/{id}")
  public Category getCategoryById(@PathVariable int id){
    Category category=catRepo.findById(id);
    return category;
  } 
  
}
