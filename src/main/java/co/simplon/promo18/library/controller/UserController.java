package co.simplon.promo18.library.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import co.simplon.promo18.library.entities.User;
import co.simplon.promo18.library.repository.UserRepository;
import org.springframework.http.HttpStatus;



@RestController
@RequestMapping("/api/user")
public class UserController {
  
  @Autowired
  private UserRepository userRepository;

  @Autowired
  private PasswordEncoder encoder;

  @GetMapping
  public List<User> getAllUser(){
    List<User> list=userRepository.findAll();
    return list;
  }
  
  @GetMapping("/account")
    public User getAccount(@AuthenticationPrincipal User user) {
        return user;
    }

  @GetMapping("/like/{search}")
  public List<User> getUserByNameLike(@PathVariable String search){
    return userRepository.findByName(search);
  }


  @GetMapping("/email/{mail}")
  public User getByEmail(@PathVariable String mail){
    return userRepository.findByEmail(mail);
  }

  @GetMapping("/{id}")
  public User getUserById(@PathVariable int id){
    User user = userRepository.getUserById(id);
    return user;
  }

  @PostMapping
  public User register(@RequestBody User user){
    user.setRole("ROLE_USER");
    // user.setRole("ROLE_ADMIN");
    
    String hashed=encoder.encode((user.getPassword()));
    user.setPassword(hashed);
    userRepository.save(user);
    return user;
  }

  @PutMapping
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public boolean completeAccount(@RequestBody User user){
    return userRepository.completeAccount(user);
  }
}
