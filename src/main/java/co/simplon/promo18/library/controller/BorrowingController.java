package co.simplon.promo18.library.controller;

import java.time.LocalDate;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import co.simplon.promo18.library.entities.Borrowing;
import co.simplon.promo18.library.entities.Copy;
import co.simplon.promo18.library.repository.BorrowingRepository;
import co.simplon.promo18.library.repository.CopyRepository;
import org.springframework.http.HttpStatus;

@RestController
@RequestMapping("/api/borrowing")
public class BorrowingController {

  @Autowired
  BorrowingRepository repo;

  @Autowired
  CopyRepository copyRepo;

  @GetMapping
  public List<Borrowing> getAll() {
    return repo.findAll();
  }

  @GetMapping("/user/{id}")
  public List<Borrowing> getBorrowingsByUserId(@PathVariable int id) {
    return repo.findBorrowingsPerUserId(id);
  }

  @GetMapping("/current/{id}")
  public List<Borrowing> getCurrentBorrowingsByUserId(@PathVariable int id) {
    return repo.findCurrentBorrowingsPerUserId(id);
  }

  @GetMapping("/{id}")
  public Borrowing getById(@PathVariable int id) {
    return repo.findById(id);
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public boolean save(@Valid @RequestBody Borrowing borrowing) {
    LocalDate localDate = LocalDate.now();
    borrowing.setBorrowingDate(localDate);
    LocalDate returningDate = localDate.plusMonths(1);
    borrowing.setExpectedDueDate(returningDate);
    int copyId = borrowing.getCopy().getId();
    Copy copy = copyRepo.findById(copyId);
    List<Borrowing> list = repo.findBorrowingsPerUserId(borrowing.getUser().getId());
    if (list.size() < 7 && copy.getStock() > 0) {
      if (repo.save(borrowing)) {
        copyRepo.modifyStock(copyId, -1);
        return true;
      }
    }
    return false;
  }

  @PutMapping
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public boolean confirmReturn(@RequestBody Borrowing borrowing,@RequestParam(required = false) String state) {
    if(state!=null){
      copyRepo.modifyState(state, borrowing.getCopy().getId());
    }
    if(repo.confirmReturnBorrowing(borrowing)){
      copyRepo.modifyStock(borrowing.getCopy().getId(), 1);
      return true;
    }
    return false;
  }
}
