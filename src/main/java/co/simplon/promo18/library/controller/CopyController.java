package co.simplon.promo18.library.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import co.simplon.promo18.library.entities.Copy;
import co.simplon.promo18.library.repository.CopyRepository;
import org.springframework.http.HttpStatus;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/copy")
public class CopyController {

  @Autowired
  private CopyRepository copyRepository;

  @GetMapping
  public List<Copy> getAllCopies(){
    List<Copy> list=copyRepository.findAll();
    return list;
  }

  @GetMapping("/{id}")
  public Copy getCopyById(@PathVariable int id){
    Copy copy=copyRepository.findById(id);
    return copy;
  }

  @GetMapping("/book/{id}")
  public List<Copy> getCopyByBookId(@PathVariable int id){
    return copyRepository.findByBookId(id);
  }

  @PatchMapping("/{nb}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void modifyStock(@RequestBody Copy copy, @PathVariable int nb){
    copyRepository.modifyStock(copy.getId(), nb);
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public void createCopy(@Valid @RequestBody Copy copy){
    copyRepository.save(copy);
  }

  @DeleteMapping("/{id}")
  public boolean deleteCopy(@PathVariable int id){
    return copyRepository.deleteById(id);
  }

  @PutMapping
  public boolean updateCopy(@RequestBody Copy copy){
    return copyRepository.update(copy);
  }

 
}
