-- Active: 1648455754338@@127.0.0.1@3306@libary
CREATE DATABASE library;
use library;

CREATE TABLE category(
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL
);

CREATE TABLE book(
  id INT PRIMARY KEY AUTO_INCREMENT,
  title VARCHAR(255) NOT NULL,
  author VARCHAR(255) NOT NULL,
  abstract TEXT NOT NULL,
  category_id INT,
  FOREIGN KEY (category_id) REFERENCES category(id) ON DELETE CASCADE
);

CREATE TABLE copy(
  id INT PRIMARY KEY AUTO_INCREMENT,
  editor VARCHAR(255) NOT NULL,
  page_number INT,
  state VARCHAR(155),
  cover VARCHAR(255),
  stock INT,
  book_id INT,
  FOREIGN KEY (book_id) REFERENCES book(id) ON DELETE CASCADE
);

create table user(
  id INT PRIMARY KEY AUTO_INCREMENT,
  first_name VARCHAR(255),
  last_name VARCHAR(255),
  email VARCHAR(255) NOT NULL UNIQUE,
  password VARCHAR(255)NOT NULL,
  role VARCHAR(255),
  birthdate DATE
);

CREATE TABLE borrowing(
  id INT PRIMARY KEY AUTO_INCREMENT,
  borrowing_date DATE,
  returning_date DATE,
  expected_due_date DATE,
  copy_id INT,
  user_id INT,
  FOREIGN KEY (copy_id) REFERENCES copy(id) ON DELETE CASCADE,
  FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE
);

INSERT INTO category (name) VALUES ('classics'),('mystery'),('fantasy'),
('history'),('horror'),('fiction'), ('thrillers');

INSERT INTO book(title, author, abstract, category_id) VALUES 
('to kill a mockingbird', 'Harper Lee','Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea officia perferendis tempora laboriosam expedita, cupiditate sequi eum ut dolorum dicta! Tempore suscipit ipsa quae quaerat quos omnis soluta vitae natus?',1),
('Little women', 'Louisa May Alcott','Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea officia perferendis tempora laboriosam expedita, cupiditate sequi eum ut dolorum dicta! Tempore suscipit ipsa quae quaerat quos omnis soluta vitae natus?',1),
('And then there were none', 'Agatha Christie','Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea officia perferendis tempora laboriosam expedita, cupiditate sequi eum ut dolorum dicta! Tempore suscipit ipsa quae quaerat quos omnis soluta vitae natus?',2),
('The adventure of Sherlock Holmes', 'Athur Conan Doyle','Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea officia perferendis tempora laboriosam expedita, cupiditate sequi eum ut dolorum dicta! Tempore suscipit ipsa quae quaerat quos omnis soluta vitae natus?',2),
('Circe', 'Madeline Miller','Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea officia perferendis tempora laboriosam expedita, cupiditate sequi eum ut dolorum dicta! Tempore suscipit ipsa quae quaerat quos omnis soluta vitae natus?',3),
('One world the water dancer','Ta -Nehisi Coates','Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea officia perferendis tempora laboriosam expedita, cupiditate sequi eum ut dolorum dicta! Tempore suscipit ipsa quae quaerat quos omnis soluta vitae natus?',3),
('The crusades', 'Thomas Asbridge','Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea officia perferendis tempora laboriosam expedita, cupiditate sequi eum ut dolorum dicta! Tempore suscipit ipsa quae quaerat quos omnis soluta vitae natus?',4),
('1776', 'David McCullough','Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea officia perferendis tempora laboriosam expedita, cupiditate sequi eum ut dolorum dicta! Tempore suscipit ipsa quae quaerat quos omnis soluta vitae natus?',4),
('Carrie', 'Stephen King','Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea officia perferendis tempora laboriosam expedita, cupiditate sequi eum ut dolorum dicta! Tempore suscipit ipsa quae quaerat quos omnis soluta vitae natus?',5),
('Bird box', 'Josh Malerman','Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea officia perferendis tempora laboriosam expedita, cupiditate sequi eum ut dolorum dicta! Tempore suscipit ipsa quae quaerat quos omnis soluta vitae natus?',5),
('Olive again', 'Elisabeth Strout','Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea officia perferendis tempora laboriosam expedita, cupiditate sequi eum ut dolorum dicta! Tempore suscipit ipsa quae quaerat quos omnis soluta vitae natus?',6),
('1984','George Orwell','Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea officia perferendis tempora laboriosam expedita, cupiditate sequi eum ut dolorum dicta! Tempore suscipit ipsa quae quaerat quos omnis soluta vitae natus?',6),
('The guardians', 'John Grisham','Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea officia perferendis tempora laboriosam expedita, cupiditate sequi eum ut dolorum dicta! Tempore suscipit ipsa quae quaerat quos omnis soluta vitae natus?',7),
('Gone girl', 'Gillian Flynn','Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea officia perferendis tempora laboriosam expedita, cupiditate sequi eum ut dolorum dicta! Tempore suscipit ipsa quae quaerat quos omnis soluta vitae natus?',7);

INSERT INTO copy( editor, page_number, state, cover, book_id, stock) VALUES
('editor name', 254, 'neuf','https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1572292658-51IXWZzlgSL.jpg?crop=1xw:0.996xh;center,top&resize=640:*',1,5 ),
('editor name2',310, 'abimé','https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1572292943-418D9yYGB3L.jpg?crop=0.900xw:1.00xh;0.0916xw,0&resize=640:*',1,5),
('editor name3',295, 'bon état','https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1572292943-418D9yYGB3L.jpg?crop=0.900xw:1.00xh;0.0916xw,0&resize=640:*',1,5),
('editor name', 550, 'abimé', 'https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1572292680-51xJASXQJwL.jpg?crop=1.00xw:0.978xh;0,0.0120xh&resize=640:*',2,5),
('editor name2', 570, 'bon état', 'https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1572292680-51xJASXQJwL.jpg?crop=1.00xw:0.978xh;0,0.0120xh&resize=640:*',2,5),
('editor name3', 545, 'neuf', 'https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1572292680-51xJASXQJwL.jpg?crop=1.00xw:0.978xh;0,0.0120xh&resize=640:*',2,5),
('editor name', 780, 'neuf', 'https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1572293489-51MlxNgCsyL.jpg?crop=1.00xw:0.932xh;0,0.0440xh&resize=640:*',3,5),
('editor name', 150, 'écorné','https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1572293468-516dZVA7rQL.jpg?crop=0.958xw:1xh;center,top&resize=640:*',4,5),
('editor name', 1000, 'neuf', 'https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1572293746-51dzuwLmm-L.jpg?crop=1.00xw:0.932xh;0,0.0540xh&resize=640:*',5,5 ),
('editor name', 950, 'léregerement abimé', 'https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1572293716-51ItgiZw5dL.jpg?crop=1xw:0.987xh;center,top&resize=640:*',6,5),
('editor name', 260, 'neuf', 'https://cdn.pixabay.com/photo/2020/06/25/09/52/crusader-5339187__480.jpg',7,5),
('editor name', 840, 'abimé', 'https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1572364763-51eGhXmi23L.jpg?crop=0.998xw:1xh;center,top&resize=640:*',8,5),
('editor name', 500, 'neuf', 'https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1572295234-51dRjo8NJeL.jpg?crop=1.00xw:0.896xh;0.00336xw,0.102xh&resize=640:*',9,5),
('editor name', 400, 'ancien', 'https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1572295284-41sRIihnXzL.jpg?crop=1xw:0.996xh;center,top&resize=640:*',10,5),
('editor name', 156, 'neuf','https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1572295502-512WyIl2uSL.jpg?crop=1xw:0.990xh;center,top&resize=640:*',11,5),
('editor name', 780, 'ancien', 'https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1572296688-410ZirPKXKL.jpg?crop=1xw:0.999xh;center,top&resize=640:*',12,5),
('editor name', 650, 'neuf','https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1572297648-51U2hIUQgqL.jpg?crop=0.972xw:1xh;center,top&resize=640:*',13,5),
('editor name', 960, 'couverture abimé', 'https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1572297615-416TH3igfOL.jpg?crop=1xw:0.972xh;center,top&resize=640:*',14,5);



INSERT INTO copy( editor, page_number, state, cover, book_id) VALUES
('editor name',700, 'abimé','https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1572292943-418D9yYGB3L.jpg?crop=0.900xw:1.00xh;0.0916xw,0&resize=640:*',1);

INSERT INTO user(first_name, last_name, email, password, role, birthdate)
 VALUES ("Valentin", "Francois", "va@va.fr", "1234", "USER", "2014-10-11"),
 ("michel", "durant", "du@mi.fr", "1234", "ADMIN", "1988-01-07");


INSERT INTO borrowing (borrowing_date, returning_date, expected_due_date, copy_id, user_id) VALUES
("2022-05-06", null, "2022-06-06", 1,1),
("2022-06-06",null,"2022-07-06",2,1);
select * from user;

select * from borrowing;

select * from copy;

update borrowing SET returning_date='2022-10-10'
WHERE id=10;

select * from book;

select * from book WHERE book.title like '%an%' OR author like '%an%' ;

select * from borrowing where returning_date IS null AND user_id=4;

SELECT * FROM borrowing;